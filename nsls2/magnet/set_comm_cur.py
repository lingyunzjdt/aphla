import numpy as np
import aphla as ap
import matplotlib.pylab as plt
from datetime import datetime
import time


def set_current(fname, fac = 1.0):
    for line in open(fname, 'r').readlines():
        if line.find("{PS:") < 0: continue
        recs = line.split()
        if len(recs) < 3: continue
        elif len(recs) == 3:
            name, pv, Ipv = recs
            kl = 0.0
        elif len(recs) == 4:
            name, kl, pv, Ipv = recs
        else:
            continue

        kl = float(kl)
        Ipv = float(Ipv)
        ap.catools.caput(pv, Ipv, wait=True)

def read_pv():
    """read the pv"""
    dat = {}
    for elem in ap.getGroupMembers(["QUAD", "SEXT"], op="union"):
        if "b1" in elem.fields():
            fld = "b1"
        elif "b2" in elem.fields():
            fld = "b2"
        pvrb = elem.pv(field=fld, handle="readback")
        pvsp = elem.pv(field=fld, handle="setpoint")
        print elem.name, pvrb, pvsp
    return dat

def plot_rb_sp(dat):
    plt.clf()
    plt.subplot(211)
    plt.plot([(r[4] - r[2])/r[2]*100 for r in dat], '-x')
    plt.label("diff [%]")
    plt.subplot(212)
    plt.plot([r[2] for r in dat], 'r-x')
    plt.plot([r[4] for r in dat], 'g-o')
    plt.savefig("wguo_commissioning.png")

def ring_par_to_cur(fname, fac = 1.0):
    dat = {}
    for line in open(fname, 'r').readlines():
        try:
            name, tp, L, s, k1, k2, theta = line.split()
            L, k1, k2 = float(L), float(k1), float(k2)
        except:
            continue
        if tp not in ["QUAD", "SEXT"]: continue
        if name.startswith("SQ"): continue

        elems = ap.getElements(name.lower())
        if not elems:
            raise RuntimeError("can not find '%s'" % name)

        elem = elems[0]
        if "b1" in elem.fields():
            fld, kl = "b1", k1*L
        elif "b2" in elem.fields():
            fld, kl = "b2", k2*L
        pvs = elem.pv(field=fld, handle="setpoint")
        assert len(pvs) == 1
        pv = pvs[0]
        Ipv1 = elem.convertUnit(fld, kl*fac, "phy", None)
        dat.setdefault(pv, [])
        dat[pv].append((name, kl, Ipv1))
    #
    f = open("pv_cur.txt", "w")
    for k,v in dat.items():
        names  = sorted([r[0] for r in v])
        kl = np.average([r[1] for r in v])
        Ipv = np.average([r[2] for r in v])
        f.write("%s %13.8f %s %13.8f \n" %(names[0], kl, k, Ipv))
    f.close()

        
def set_aphla(fname, fac = 1.0):
    diff = []
    for line in open(fname, 'r').readlines():
        try:
            name, tp, L, s, k1, k2, theta = line.split()
            L, k1, k2 = float(L), float(k1), float(k2)
        except:
            continue
        if tp not in ["QUAD", "SEXT"]: continue
        if name.startswith("SQ"): continue

        elems = ap.getElements(name.lower())
        if not elems:
            raise RuntimeError("can not find '%s'" % name)
        elem = elems[0]
        if "b1" in elem.fields():
            fld, kl = "b1", k1*L
        elif "b2" in elem.fields():
            fld, kl = "b2", k2*L
        pv = elem.pv(field=fld, handle="setpoint")
        Ipv0 = elem.get(fld, unitsys=None)
        Ipv1 = elem.convertUnit(fld, kl*fac, "phy", None)
        print name, L, kl, elem.get(fld), Ipv1 - Ipv0
        #elem.put(fld, np.abs(float(kl)*fac))
        ap.catools.caput(pv[0], Ipv1)
        diff.append([name, Ipv0, Ipv1])
    d = []
    for i,r in enumerate(diff):
        if r[0].startswith("Q"):
            d.insert(0, r[1:])
        elif r[0].startswith("S"):
            d.append(r[1:])
    d = np.array(d, 'd')
    plt.clf()
    plt.plot((d[:,1] - d[:,0])/d[:,0] * 100.0)
    plt.ylabel("(aphla - W.Guo)/W.Guo [%%]")
    plt.savefig("tmp.png")

def _turn_ps(fname, onoff):
    import aphla as ap
    ap.machines.load("nsls2")
    for line in open(fname, 'r').readlines():
        try:
            name, tp, L, s, k1, k2, theta = line.split()
            L, k1, k2 = float(L), float(k1), float(k2)
        except:
            continue
        if tp not in ["QUAD", "SEXT"]: continue
        if name.startswith("SQ"): continue

        elems = ap.getElements(name.lower())
        if not elems:
            raise RuntimeError("can not find '%s'" % name)
        elem = elems[0]
        if "b1" in elem.fields():
            fld, kl = "b1", k1*L
        elif "b2" in elem.fields():
            fld, kl = "b2", k2*L
        pv = elem.pv(field=fld, handle="setpoint")[0]
        #
        pvon = pv.replace("I:Sp1-SP", "PsOnOff-Sel")
        # turn off - 0
        ap.catools.caput(pvon, 0, wait=True)
        # turn On - 1
        #ap.catools.caput(pvon, 1, wait=True)
        print pv, ap.catools.caget(pvon)

def turn_ps_off():
    _turn_ps("wguo_comm-ring-par.txt", 0)

def turn_ps_on():
    _turn_ps("wguo_comm-ring-par.txt", 1)

def cmp_pv_cur():
    dat = {}
    for line in open("yli_pv_cur.txt", 'r').readlines()[2:]:
        name, kl, pv, Ipv = line.split()
        dat.setdefault(pv, [])
        dat[pv].append(float(Ipv))

    for line in open("wguo_comm-ring-pv-cur.txt", 'r').readlines():
        if line.find("SR:") < 0 or line.find("PS") < 0: continue
        recs = line.split()
        if len(recs) != 3: continue
        name, pv, Ipv = recs
        Ipv = np.abs(float(Ipv)/0.9988)
        dat[pv].append(Ipv)

    for line in open("pv_cur.txt", 'r').readlines():
        name, kl, pv, Ipv = line.split()
        dat[pv].append(float(Ipv))

    idx = []
    for k,v in dat.items():
        if k.find("PS:Q") > 0:
            idx.insert(0, k)
        elif k.find("PS:S") > 0:
            idx.append(k)
        else:
            raise RuntimeError("unknown PV %s" % k)
    
    d = np.array([dat[k] for k in idx])
    diff1  = d[:,0] - d[:,-1]
    diff2  = d[:,1] - d[:,-1]
    rdiff1 = (d[:,0] - d[:,-1])/d[:,-1]*100.0
    rdiff2 = (d[:,1] - d[:,-1])/d[:,-1]*100.0

    plt.clf()
    plt.subplot(2,1,1)
    plt.plot(diff1, 'r-', label="Y.Li")
    plt.plot(diff2, 'g-', label="W.Guo")
    plt.subplot(2,1,2)
    plt.plot(rdiff1, 'r-', label="Y.Li")
    plt.plot(rdiff2, 'g-', label="W.Guo")
    plt.savefig("tmp.png")

def wait_nonfault(fname, tmax = 10):
    t0 = datetime.now()
    pvsts = []
    for line in open(fname, 'r').readlines():
        if line.find("{PS:") < 0: continue
        recs = line.split()
        if len(recs) < 3: continue
        elif len(recs) == 3:
            name, pv, Ipv = recs
            kl = 0.0
        elif len(recs) == 4:
            name, kl, pv, Ipv = recs
        else:
            continue
        pvsts.append(pv.replace("I:Sp1-SP", "Sum1-Sts"))

    while True:
        st = np.sum(ap.catools.caget(pvsts))
        if st == 0: return True
        t1 = datetime.now()
        if (t1-t0).total_seconds() > tmax:
            return False


if __name__ == "__main__":
    ap.enableLog()
    #ap.machines.load("nsls2")
    #set_current(fmult)
    #wait_nonfault("comm_pv_cur_lyang.txt")
    set_current("comm_pv_cur_lyang.txt")
    #set_aphla("wguo_comm-ring-par.txt", 1.0/0.9988)
    #turn_ps_off()
    #ring_par_to_cur("wguo_comm-ring-par.txt")
    #cmp_pv_cur()
