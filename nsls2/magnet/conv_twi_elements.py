import sys
import re
import subprocess
#import commands
import sqlite3
import aphla as ap


def write_pv_table(fname, pvs):
    out = open(fname, "w")
    for i,pv in enumerate(pvs):
        m = re.match(r"BTS-MG\{(\w+):(\d+)\}I:(.+)", pv)
        if not m:
            print pv
            continue
        fam, idx, pf = "", "", ""
        if m:
            fam, idx, pf = m.group(1), m.group(2), m.group(3)
        hdl = "get"
        if pf.endswith("-SP") > 0: hdl = "put"

        if pv.find("{Bend:") > 0:
            name, fld = "B" + pv[12], "b0"
        elif pv.find("{Cor:") > 0:
            name = "C" + pv[11]
            if pv.find("Ps1") > 0 or pv.find("Sp1-") > 0:
                fld = 'x'
            elif pv.find("Ps2") > 0 or pv.find("Sp2-") > 0:
                fld = 'y'
            else:
                fld = ''
        elif pv.find("{Quad:") > 0:
            name, fld = "Q" + pv[12], "b1"
        else:
            name, fld = "", ""
        out.write(",".join([pv,hdl,name,"","",fld]) + "\n")
    out.close()

if __name__ == "__main__":
    # read the twiss from Elegant
    #pvs = [s.strip() for s in open('bts_pvlist.txt', 'r').readlines()]
    #write_pv_table("bts_pvs.txt", pvs)
    ap.machines.utils.convElegantToSqlite(
        "nsls2_bts.sqlite", "BSR_Model.twi", fpv="bts_pvs.txt", latname="BTS")


