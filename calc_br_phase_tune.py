import sys
import h5py
import numpy as np
from numpy import pi
import matplotlib.pylab as plt
from notebooks import nsls2

#Phase: [ 0.39749762  1.37881292  2.19068184  2.51026514  2.75745336  3.84711391]
#Phase: [ 0.73669081  0.82941541  1.08921062  1.49682582  1.88287816  2.81783745]

def test(N = 2):
    np.random.seed(2)
    nbpm, nturn = 6, 1024
    for i in range(N):
        phase = np.zeros((nbpm+1, 3), 'd')
        xtbt = np.zeros((nbpm, nturn), 'd')
        r = np.random.rand(nbpm + 1) * 0.45
        nu = np.sum(r) - r[0] # range [9,9+3)
        print "The Tune:", nu, nu*pi*2
        phase[:,0] = r * pi * 2.0
        phase[:,1] = np.add.accumulate(phase[:,0])
        for j in range(nbpm):
            t = np.arange(nturn)
            xtbt[j,:] = np.cos(2*np.pi*nu*t + phase[j,1])
            print j, "Phi= %.4f s=%.4f c=%.4f t=%.4f" % (
                phase[j,1], np.sin(phase[j,1]),
                np.cos(phase[j,1]), np.tan(phase[j,1]))
        phase[:-1,2] = nsls2.calcPhase(xtbt, nu=nu, accumulate=True)
        print "Phase 0:", phase[:,0]
        print "Phase 1:", phase[:,1]
        print "Phase 2:", phase[:,2]
        print "Phase Advance:", phase[:,1] - phase[0,1]
        print "Phase Advance:", phase[:,2] - phase[0,2]
        plt.subplot(N, 3, 3*i + 1)
        plt.plot(phase[:,0], '-x')
        plt.subplot(N, 3, 3*i + 2)
        plt.plot(phase[:,1] - phase[0,1], '-x')
        plt.plot(phase[:,2] - phase[0,2], '-o')
        plt.subplot(N, 3, 3*i + 3)
        plt.plot(phase[1:,1] - phase[:-1,1], '-x')
        plt.plot(phase[1:,2] - phase[:-1,2], '-o')

        phi = nsls2.calcPhaseAdvance(xtbt)
        print "Tune:", np.sum(phi), nu

    plt.savefig("test.png")


def calcPhaseTune(fname, h5group):
    f = h5py.File(fname, 'r')
    g = f[h5group]
    if "Tbt_x" in g:
        xtbt = np.array(g["Tbt_x"], 'd')
    elif "data_tbt_x" in g:
        xtbt = np.array(g["data_tbt_x"], 'd')
    #ph = nsls2.calcPhase(xtbt)
    ph = nsls2.calcPhaseAdvance(xtbt[:,1024:2048])
    #print "Phase Advance:", ph[-1] - ph[0]
    #print "Phase Advance:", (ph[-1] - ph[0]) / 2.0 / np.pi
    print "Tune:", np.sum(ph) / 2.0 / np.pi
    print ph / 2.0 / np.pi

def calcPhaseTunes(fname):
    # get a list of dataset/group
    f = h5py.File(fname, 'r')
    L = []
    f.visit(L.append)
    for ds in L:
        try:
            if f[ds].get("Tbt_x", None):
                xtbt = np.array(f[ds]["Tbt_x"], 'd')
            elif f[ds].get("data_tbt_x", None):
                xtbt = np.array(f[ds]["data_tbt_x"], 'd')
            else:
                xtbt = None
        except:
            continue
        if xtbt is None: continue
        ph1 = nsls2.calcPhaseAdvance(xtbt[:,1024:2048])
        ph2 = nsls2.calcPhase(xtbt[:,1024:2048], debug=1)
        print ds, "Tune:", np.sum(ph1) / 2.0 / np.pi, (ph2[-1] - ph2[0])/2.0/np.pi
        #print ph / 2.0 / np.pi
        #plt.plot(ph, '-o')
    #plt.savefig("phase.png")
    f.close()


if __name__ == "__main__":
    #test()
    #calcPhaseTune(sys.argv[1], sys.argv[2])
    calcPhaseTunes(sys.argv[1])
