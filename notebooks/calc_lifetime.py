import numpy as np
import aphla as ap
import time
from collections import deque
from datetime import datetime
import matplotlib.pylab as plt


def calc_bpm_sum(nbuf, nloop, nt):
    bpms = ap.getElements("BPM")
    dcct = ap.getElements("DCCT")[0]
    i, n = 0, 0
    dat = np.zeros((nbuf, 180), 'd')
    xt = np.zeros(nbuf, 'd')
    t0 = datetime.now()
    while i < nloop:
        dt = (datetime.now() - t0).total_seconds()
        if i < nbuf:
            dat[i,:] = ap.fget("BPM", "ampl")
            xt[i] = dt
        else:
            dat[:-1,:] = dat[1:,:]
            dat[-1,:] = ap.fget("BPM", "ampl")
            xt[:-1] = xt[1:]
            xt[-1] = dt
            i1 = nt
        i += 1
        if i < 4: continue
        if i < nt:
            p, residuals, rank, sv, rcond = \
                np.polyfit(xt[:i], dat[:i,:], 1, full=True)
            tau = -dat[i-1,:]/p[-2,:]/3600.0
            plt.plot(xt[:i], dat[:i,:], '-')
        elif i < nbuf:
            p, residuals, rank, sv, rcond = \
                np.polyfit(xt[i-nt:i], dat[i-nt:i,:], 1, full=True)
            tau = -dat[i-1,:]/p[-2,:]/3600.0
            plt.plot(xt[i-nt:i], dat[i-nt:i,:], '-')
        else:
            p, residuals, rank, sv, rcond = \
                np.polyfit(xt[-nt:], dat[-nt:,:], 1, full=True)
            tau = -dat[-1,:]/p[-2,:]/3600.
            plt.plot(xt[-nt:], dat[-nt:,:], '-')
        print i, xt[-1], np.average(tau), np.std(tau), dcct.I
        #print np.shape(p)
        plt.savefig("tmp.png")
        time.sleep(3)


if __name__ == "__main__":
    ap.machines.load("nsls2", "SR")
    calc_bpm_sum(10, 500, 3)
