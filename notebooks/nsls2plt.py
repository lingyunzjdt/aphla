"""
NSLS-II Plotting
================

:author: Lingyun Yang
:date: 2014-03-27 22:10:02
"""

import h5py
import numpy as np
import matplotlib.pylab as plt
from datetime import datetime

def pltCaRm(fname, **kwargs):
    f = h5py.File(fname, 'r')
    for kker in kwargs.get("group", f.keys()):
        g = f[kker]
        if not isinstance(g, h5py.Group): continue
        dxlst    = g["dxlst"]
        raw_data = g["raw_data"]
        nx, nresp, nsample = np.shape(raw_data)
        # make nxlst*nsample 1-D to do the fitting
        x = np.zeros(nx*nsample, 'd')
        y = np.zeros((nx*nsample, nresp), 'd')
        for i in range(nx):
            x[i*nsample:(i+1)*nsample] = dxlst[i]
            y[i*nsample:(i+1)*nsample,:] = np.transpose(raw_data[i,:,:])
        p, res, rank, sv, rcond = np.polyfit(x, y, 1, full = True)
        #print p, res
        plt.subplot(3,1,1)
        plt.plot(np.sum(np.std(raw_data, axis=2), axis=0), 'x-')
        plt.ylabel("sum(std)")
        plt.subplot(3,1,2)
        plt.plot(p[0,:], 'x-')
        plt.ylabel("slope")
        plt.subplot(3,1,3)
        plt.plot(res, 'x-')
        plt.ylabel("residuals")
