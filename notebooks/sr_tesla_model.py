import tesla
import aphla as ap
import matplotlib.pylab as plt
import time

ap.machines.load("nsls2", "SR")

ap.nsls2.saveLattice(notes="remote saving")
sr = tesla.Ring("nsls2_comm_ring.tslat", "RING")
tw = sr.twiss()
print tw.tunes()

print "Reading QUAD"
iquad = sr.matchElements("Q[HLM][0-9].*")
assert len(iquad) == 300
qls = ap.getElements("QUAD")
k1l = ap.fget("QUAD", "b1", handle="readback", unitsys="phy")
for i,iq in enumerate(iquad):
    #sr[iq,"k1"] = k1l[i]/qls[i].length
    #sr[iq, "k1"] = qls[i].get("b1", handle="golden")/qls[i].length
    print qls[i].name, iq, sr.elementName(iq)
    #b1l.append(qls[i].get("b1") - qls[i].get("b1", handle="golden"))
    #print qls[i].get("b1", handle="setpoint")#k1l[i]
    #sr[iq,"k1"] = qls[i].get("b1", handle="golden")/qls[i].length
    sr[iq,"k1"] = qls[i].get("b1", handle="readback")/qls[i].length
tw2 = sr.twiss()
print tw2.tunes()

icx = sr.matchElements("C[HLM][0-9]XG.*")
assert len(icx) == 180
cxl = ap.fget("COR", "x", handle="readback", unitsys="phy")
for i,ic in enumerate(icx):
    #print sr.elementName(i), sr[i, "hkick"]
    #sr[ic,"hkick"] = cxl[i] / 1000.0
    pass

icy = sr.matchElements("C[HLM][0-9]YG.*")
assert len(icy) == 180
cyl = ap.fget("COR", "y", handle="readback", unitsys="phy")
for i,ic in enumerate(icy):
    sr[ic,"vkick"] = cyl[i]/1000.0

cob = sr.closedOrbit()
print len(cob), len(cob[0])
print cob[0]
print cob[-1]

plt.clf()
plt.plot([v[4] for v in cob], [v[0] for v in cob], 'g-')
plt.plot([v[4] for v in cob], [v[2] for v in cob], 'b-')
plt.savefig("tmp.png")
#isext = sr.matchElements("S[HLM][0-9].*")
#assert len(isext) == 300
#plt.savefig("tmp.png")

obt = ap.getOrbit(spos=True)
plt.clf()
plt.plot(obt[:,-1], obt[:,0], '-', label="X [mm]")
plt.plot(obt[:,-1], obt[:,1], '-', label="Y [mm]")
plt.legend(loc="best")
plt.savefig("tmp.png")

