import h5py
import sys
import re
import numpy as np
import tesla
import matplotlib.pylab as plt
import time

#ap.machines.load("nsls2", "SR")


def init_sr(sr, fname):
    print "Reading QUAD"
    h5f = h5py.File(fname, 'r')
    h5g = h5f["SR"]
    val_phy = {}
    for pv in h5g.keys():
        for k,v in h5g[pv].attrs.items():
            if k in ["datetime", "timestamp", "ok"]: continue
            val_phy[k] = v
    h5f.close()

    for k,v in val_phy.items():
        try:
            name, fld, unitsys = k.split(".")
        except:
            print "ERROR:", k
            continue
        ltename = name.upper()
        if re.match("Q[HLM][0-9].*", ltename):
            ilst = sr.matchElements(ltename)
            assert len(ilst) == 1
            i = ilst[0]
            sr[i, "k1"] = v / sr[i, "L"]
        elif re.match("C[HLM][0-9].*", ltename):
            if fld == 'x':
                ilst = sr.matchElements(ltename[:3] + "X" + ltename[3:])
                assert len(ilst) == 1
                sr[ilst[0], "hkick"] = v / 1000.0
            elif fld == "y":
                ilst = sr.matchElements(ltename[:3] + "Y" + ltename[3:])
                assert len(ilst) == 1
                sr[ilst[0], "vkick"] = v / 1000.0
        else:
            pass

    print val_phy


def read_orbit(sr, fname):
    ibpm = sr.matchElements("P[HLM][0-9].*")
    assert len(ibpm) == 180
    obt = np.zeros((180, 3), 'd')
    s = 0.0
    for i in range(sr.elements()):
        try:
            s = s + sr[i, "L"]
        except:
            continue
        if i in ibpm:
            j = ibpm.index(i)
            obt[j,2] = s

    h5f = h5py.File(fname, 'r')
    h5g = h5f["SR"]
    val_phy = {}
    for pv in h5g.keys():
        for k,v in h5g[pv].attrs.items():
            if k in ["datetime", "timestamp", "ok"]: continue
            name, fld, phy = k.split(".")
            val_phy["%s.%s" % (name, fld)] = v
    h5f.close()

    for i,ib in enumerate(ibpm):
        name = sr.elementName(ib)
        obt[i,0] = val_phy["%s.x" % name.lower()]
        obt[i,1] = val_phy["%s.y" % name.lower()]
    return obt

if __name__ == "__main__":
    fname = '2014_04_10_223752.hdf5'
    sr = tesla.Ring("nsls2_comm_ring.tslat", "RING")
    init_sr(sr, fname)
    
    tw = sr.twiss()
    print tw.tunes()

    obt = read_orbit(sr, fname)
    plt.subplot(2,1,1)
    plt.plot(obt[:,-1], obt[:,0], '-', label="X [mm]")
    plt.plot(obt[:,-1], obt[:,1], '-', label="Y [mm]")
    plt.subplot(2,1,2)
    dbpm = obt[1:,:] - obt[:-1,:]
    dd = np.take(dbpm, range(11, len(dbpm)-1, 12), axis=0) 
    sbpm = np.take(obt[:,-1], range(11, len(dbpm)-1, 12)) 
    print len(dd), len(sbpm)
    plt.plot(sbpm, dd[:,0]/dd[:,-1], 'x-', label="X' [mrad]")
    plt.plot(sbpm, dd[:,1]/dd[:,-1], 'o-', label="Y' [mrad]")
    plt.legend(loc="best")
    plt.savefig("tmp.png")
