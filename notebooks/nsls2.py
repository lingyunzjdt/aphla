"""
NSLS2 Booster Ring Specific Routines
============================================

:Author: Lingyun Yang
:Date: 2014-01-24 10:42:08

br - Booster Ring
"""

import os
import sys
import time
from datetime import datetime
import numpy as np
import h5py
import re

import aphla as ap
from aphla import getTwiss, getTunes

from cothread.catools import caget, caput

import matplotlib.pylab as plt

def _run_from_ipython():
    try:
        __IPYTHON__
        return True
    except NameError:
        return False

def _get_twiss(cors, plane):
    tw = {"s": [], "Alpha": [], "Beta": [],
          "Phi": [], "dPhi": [0.0]}
    tunes = ap.getTunes(source="database")
    twf = ap.getTwiss([c.name for c in cors], 
                   ["s", "alphax", "alphay", "betax", "betay",
                    "phix", "phiy", "etax"])
    assert plane in ["X", "Y", 'x', 'y'], "Plane must be 'X' or 'Y'"
    tw["dPhi"] = [0.0 for v in twf]
    if plane in ["X", 'x']:
        nu = tunes[0]
        tw["s"]     = [v[0] for v in twf]
        tw["Alpha"] = [v[1] for v in twf]
        tw["Beta"]  = [v[3] for v in twf]
        tw["Phi"]   = [v[5] for v in twf]
    elif plane in ["Y", 'y']:
        nu = tunes[1]
        tw["s"]     = [v[0] for v in twf]
        tw["Alpha"] = [v[1] for v in twf]
        tw["Beta"]  = [v[3] for v in twf]
        tw["Phi"]   = [v[5] for v in twf]
    
    for i in range(1, len(twf)):
        dph = tw["Phi"][i] - tw["Phi"][i-1]
        if dph < 0.0:
            dph += 2.0*3.1415926*nu
        tw["dPhi"][i] = dph

    return tw


def calc3CorBump(cors, **kwargs):
    """
    """
    dImax = kwargs.pop("dImax", 1.0)
    field = kwargs.pop("field", "x")
    assert len(cors) == 3, "Need exact 3 cors"
    tw = _get_twiss(cors, field)
    bta, dph = tw['Beta'], tw["dPhi"]
    #print bta, dph
    cx21 = -np.sqrt(bta[0]/bta[1])*(np.sin(dph[2] - dph[0]) / 
                                    np.sin(dph[2] - dph[1]))
    cx31 = -np.sqrt(bta[0]/bta[2])*(np.sin(dph[1] - dph[0]) / 
                                    np.sin(dph[1] - dph[2]))

    fc = [1.0, cx21, cx31]
    ibase = np.argmax(np.abs(fc))
    dI = [v/fc[ibase]*dImax for v in fc]
    #if kwargs.get("setmachine", False):
    #    for i,c in enumerate(cors):
    #        c.put(field, dI[i], unitsys=None)
    return dI


if __name__ == "__main__":
    ap.machines.load("nsls2", "SR")
    hcor = ap.getElements("HCOR")
    for i in range(10, 100):
        calc3CorBump(hcor[i:i+3], field="x")
