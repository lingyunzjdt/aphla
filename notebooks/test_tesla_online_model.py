import tesla
import aphla as ap
import matplotlib.pylab as plt
import time

ap.machines.load("nsls2", "SR")

sr = tesla.Ring("nsls2_comm_ring.tslat", "RING")
tw = sr.twiss()
print tw.tunes()

print "Reading QUAD"
iquad = sr.matchElements("Q[HLM][0-9].*")
assert len(iquad) == 300
qls = ap.getElements("QUAD")
b1l = []
while True:
    time.sleep(2)
    k1l = ap.fget("QUAD", "b1", handle="readback", unitsys="phy")
    #print k1l
    for i,iq in enumerate(iquad):
        #sr[iq,"k1"] = k1l[i]/qls[i].length
        #sr[iq, "k1"] = qls[i].get("b1", handle="golden")/qls[i].length
        print qls[i].name
        b1l.append(qls[i].get("b1") - qls[i].get("b1", handle="golden"))
        #print qls[i].get("b1", handle="setpoint")#k1l[i]
        sr[iq,"k1"] = qls[i].get("b1", handle="golden")/qls[i].length
    tw2 = sr.twiss()
    print tw2.tunes()
    break
print max(b1l), min(b1l)

#isext = sr.matchElements("S[HLM][0-9].*")
#assert len(isext) == 300
#plt.savefig("tmp.png")

