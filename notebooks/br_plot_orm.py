import os, sys
import matplotlib.pylab as plt
import numpy as np
import time
import h5py
from datetime import datetime
import re
import aphla as ap
import nsls2

ap.machines.load("nsls2")
ap.machines.use("BR")

chset = [
('ISCXW2', 'BR:IS-PS{6A:CXW2}I-SP'),
('A1CX1', 'BR:A1-PS{6A:CX1}I-SP'),
('A1CX2', 'BR:A1-PS{6A:CX2}I-SP'),
('A1CX3', 'BR:A1-PS{6A:CX3}I-SP'),
('DSCXW1', 'BR:DS-PS{6A:CXW1}I-SP'),
('DSCXW2', 'BR:DS-PS{6A:CXW2}I-SP'),
('A2CX1', 'BR:A2-PS{6A:CX1}I-SP'),
('A2CX2', 'BR:A2-PS{6A:CX2}I-SP'),
('A2CX3', 'BR:A2-PS{6A:CX3}I-SP'),
('XSCXW1', 'BR:XS-PS{6A:CXW1}I-SP'),
('XSCXW2', 'BR:XS-PS{6A:CXW2}I-SP'),
('A3CX1', 'BR:A3-PS{6A:CX1}I-SP'),
('A3CX2', 'BR:A3-PS{6A:CX2}I-SP'),
('A3CX3', 'BR:A3-PS{6A:CX3}I-SP'),
('CSCXW1', 'BR:CS-PS{6A:CXW1}I-SP'),
('CSCXW2', 'BR:CS-PS{6A:CXW2}I-SP'),
('A4CX1', 'BR:A4-PS{6A:CX1}I-SP'),
('A4CX2', 'BR:A4-PS{6A:CX2}I-SP'),
('A4CX3', 'BR:A4-PS{6A:CX3}I-SP'),
('ISCXW1', 'BR:IS-PS{6A:CXW1}I-SP'),
('A1CY1', 'BR:A1-PS{6A:CY1}I-SP'),
('A1CY2', 'BR:A1-PS{6A:CY2}I-SP'),
('A1CY3', 'BR:A1-PS{6A:CY3}I-SP'),
('A1CY4', 'BR:A1-PS{6A:CY4}I-SP'),
('A2CY1', 'BR:A2-PS{6A:CY1}I-SP'),
('A2CY2', 'BR:A2-PS{6A:CY2}I-SP'),
('A2CY3', 'BR:A2-PS{6A:CY3}I-SP'),
('A2CY4', 'BR:A2-PS{6A:CY4}I-SP'),
('A3CY1', 'BR:A3-PS{6A:CY1}I-SP'),
('A3CY2', 'BR:A3-PS{6A:CY2}I-SP'),
('A3CY3', 'BR:A3-PS{6A:CY3}I-SP'),
('A3CY4', 'BR:A3-PS{6A:CY4}I-SP'),
('A4CY1', 'BR:A4-PS{6A:CY1}I-SP'),
('A4CY2', 'BR:A4-PS{6A:CY2}I-SP'),
('A4CY3', 'BR:A4-PS{6A:CY3}I-SP'),
('A4CY4', 'BR:A4-PS{6A:CY4}I-SP')]


# response matrix, rows=(all x, all y), columns=(all h, all v)
nbpm = 36
m = np.zeros((2*nbpm, len(chset)), 'd')


#for ich in range(len(chset)):
for ich in [28,29,30,31]:
    #if ich in [28,]: continue
    name, pvsp = chset[ich]
    ndx, npt, wfm = 4, 4, "Tbt"
    fname = "br_orm_1_%03d_%s.hdf5" % (ich, name)
    print ich, fname, pvsp
    if not os.path.exists(fname): continue
    #t0 = datetime.now()
    nsls2.measBrCaRmCol(pvsp, verbose=1, output_file=fname, dxmax=0.04, timeout=8, ndx=ndx, npoints=npt, waveform=wfm, wait=3, sleep=9, trig=1)
    #nsls2.plotBrRmData(fname, pvsp, i0 = 5, i1 = 20, ndx=ndx, npoints=npt, waveform=wfm)
    #t1 = datetime.now()
    #print "dT= %.2f s" % ( (t1-t0).total_seconds(), )
    #p = nsls2.calcBrRmCol(fname, pvsp, plot=False, waveform=wfm, wfmslice=(800, 1000), output="images/br_orm_%03d_%s" % (ich, name))
    #m[:nbpm,ich] = [pi[0] for pi in p]
    #m[nbpm:,ich] = [pi[1] for pi in p]

plt.clf()
plt.imshow(m, interpolation="nearest")
plt.colorbar()
plt.savefig("images/br_orm_m.png")

u, s, v = np.linalg.svd(m)
plt.clf()
plt.semilogy(s/s[0], 'x-')
#plt.show()


nx, ny = np.shape(m)
f = open("m.txt", "w")
for i in range(nx):
    for j in range(ny):
        f.write(" %.3e" % m[i,j])
    f.write("\n")
f.close()


# In[ ]:



