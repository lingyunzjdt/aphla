ipython nbconvert --to latex --post PDF --SphinxTransformer.author="Lingyun Yang <lyyang@bnl.gov>" $1
ipython nbconvert --to latex --post PDF --SphinxTransformer.author="Lingyun Yang <lyyang@bnl.gov>" $1

rt=`basename $1 .ipynb`
output=${rt}_`date +%Y_%m%d_%H%M%S`.pdf
mv ${rt}.pdf ${output}
