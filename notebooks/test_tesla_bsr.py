import tesla
import aphla as ap
import matplotlib.pylab as plt

#ap.machines.load("nsls2", "SR")

sr = tesla.Ring("BSR20140305.tslat", "BSR")
ihcors = sr.matchElements("C[HLM][0-9]X.*")
assert len(ihcors) == 180
#cx = ap.fget("COR", 'x', handle="setpoint", unitsys="phy")
assert len(cx) == 180
for i,ic in enumerate(ihcors):
    #c = ap.getElements("COR")[i]
    sr[ic,"hkick"] = cx[i]# c.get("x")
    #print c.name, sr.elementName(ic), c.get('x')

print "Reading QUAD"
iquad = sr.matchElements("Q[HLM][0-9].*")
assert len(iquad) == 300
qls = ap.getElements("QUAD")
#k1l = ap.fget("QUAD", "b1", handle="setpoint", unitsys="phy")
for i,iq in enumerate(iquad):
    #sr[iq,"k1"] = k1l[i]/qls[i].length
    sr[iq, "k1"] = qls[i].get("b1", handle="golden")/qls[i].length
tw2 = sr.twiss()
print tw2.tunes()
obt2 = sr.closedOrbit()
plt.plot(obt2[:,4], obt2[:,0], 'g-')
plt.plot(obt2[:,4], obt2[:,2], 'g--')

isext = sr.matchElements("S[HLM][0-9].*")
assert len(isext) == 300

plt.savefig("tmp.png")

