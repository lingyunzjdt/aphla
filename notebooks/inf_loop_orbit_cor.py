import matplotlib.pylab as plt
import aphla as ap
import numpy as np
import time
from datetime import datetime

ap.machines.load("nsls2", "SR")

x0 = np.array(ap.fget("BPM", "x0", unitsys=None))
y0 = np.array(ap.fget("BPM", "x0", unitsys=None))

cx0 = np.array(ap.fget("COR", "x", unitsys=None))
cy0 = np.array(ap.fget("COR", "y", unitsys=None))

fig = plt.figure(figsize=(16,4))
i, t0 = 0, datetime.now()
while True:
    x1 = np.array(ap.fget("BPM", "x0", unitsys=None))
    y1 = np.array(ap.fget("BPM", "y0", unitsys=None))

    cx1 = np.array(ap.fget("COR", "x", unitsys=None))
    cy1 = np.array(ap.fget("COR", "y", unitsys=None))
    
    plt.clf()
    plt.subplot(2,1,1)
    plt.plot(x1 - x0, 'r-')
    plt.plot(y1 - y0, 'g-')
    plt.grid(True)
    plt.subplot(2,1,2)
    plt.plot(cx1 - cx0, 'r-')
    plt.plot(cy1 - cy0, 'g-')
    plt.grid(True)
    plt.savefig("inf_loop_orbit_cor_diff.png")
    i = i + 1
    t1 = datetime.now()
    print i, np.std(x1-x0), np.std(y1-y0), (t1-t0).total_seconds()
    
    time.sleep(3)


